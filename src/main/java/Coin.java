/**
 * Created by dsuyang on 3/30/17.
 * A factory method to produce coins with specified weight and size.
 */
public class Coin implements Constants {
    private String weight;
    private String size;
    public String GetWeight(){return weight;}
    public String GetSize(){return size;}


    public Coin(String weight, String size)
    {
        this.weight = weight;
        this.size = size;
    }

    /**
     * The factory method to produce coin.
     * @param coinType type of the coin.
     * @return an instance of the coin.
     */
    public static Coin make_coin(String coinType)
    {
        switch (coinType) {
            case NICKEL:
                return new Coin(NICKEL_WEIGHT, NICKEL_SIZE);
            case DIME:
                return new Coin(DIME_WEIGHT, DIME_SIZE);
            case QUARTER:
                return new Coin(QUARTER_WEIGHT, QUARTER_SIZE);
            case PENNY:
                return new Coin(PENNY_WEIGHT, PENNY_SIZE);
            case FAKE:
            default:
                return new Coin(UNDEFINED_WEIGHT, UNDEFINED_SIZE);
        }
    }

    /**
     * Recognize the type of the coin.
     * @param coin a coin instance with specified weight and size.
     * @return The type of the coin.
     */
    public static String recognize_coin(Coin coin)
    {
        if (coin.GetWeight() == Constants.NICKEL_WEIGHT && coin.GetSize() == Constants.NICKEL_SIZE)
            return NICKEL;
        else if ( coin.GetWeight() == Constants.DIME_WEIGHT && coin.GetSize() == Constants.DIME_SIZE)
            return DIME;
        else if (coin.GetWeight() == Constants.QUARTER_WEIGHT && coin.GetSize() == Constants.QUARTER_SIZE)
            return QUARTER;
        else if (coin.GetWeight() == Constants.PENNY_WEIGHT && coin.GetSize() == Constants.PENNY_SIZE)
            return PENNY;
        else
            return FAKE;
    }
    public static float recognize_coin_value(String type) {

        switch (type)
        {
            case NICKEL:
                return NICKEL_VALUE;
            case DIME:
                return DIME_VALUE;
            case QUARTER:
                return QUARTER_VALUE;
            default:
                return 0.f;
        }
    }
    public static float recognize_coin_value(Coin coin) {
        return recognize_coin_value(recognize_coin(coin));
    }
}
