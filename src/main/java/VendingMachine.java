import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by dsuyang on 3/30/17.
 * The class that handles the I/O of the vending machine and inventories.
 */
public class VendingMachine implements Constants {

    private HashMap<String, Integer> inventories = new HashMap<>();
    private Register register = new Register();
    private String dispersion = Empty;
    private String displayState = INSERT_COIN;
    private String prevKeyStrike = "";

    public VendingMachine() {
        ClearInventories();
        register.ClearRegister();
    }

    //region Vending Machine UI

    /**
     * Accept keyboard input.
     *
     * @param keyStrike it can be a product name or return coins.
     */
    public void PressKeyboard(String keyStrike) {
        prevKeyStrike = keyStrike;
        switch (keyStrike) {
            case Key_Cola:
            case Key_Chips:
            case Key_Candy:
                String product = Product_Keys.get(keyStrike);
                float productPrice = Products.get(product);
                if (inventories.get(product) == null || inventories.get(product) == 0)
                    displayState = SOLD_OUT;
                else if (register.GetCoinsValueInReservoir(CREDIT) < productPrice)
                    displayState = PRODUCT_PRICE;
                else {
                    dispersion = product;
                    displayState = THANK_YOU;
                    inventories.put(product, inventories.get(product) - 1);
                    register.Buy(product);
                }
                break;
            case Key_Return:
                displayState = INSERT_COIN;
                dispersion = Empty;
                register.ReturnCredits();
                break;
            default:
                dispersion = Empty;
                break;
        }
    }


    /**
     * The main interface to the user showing the reminder message
     *
     * @return message displayed on the screen.
     */
    public String GetDisplay() {
        switch (displayState) {
            case INSERT_COIN:
                return ShowInsertOrExactChange();
            case THANK_YOU:
                displayState = INSERT_COIN;
                return THANK_YOU_MESSAGE;
            case CURRENT_AMOUNT:
                return String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                        DecimalFormat.getCurrencyInstance().format(register.GetCoinsValueInReservoir(CREDIT)));
            case SOLD_OUT:
                if (register.GetCoinsValueInReservoir(CREDIT) == 0.f)
                    displayState = INSERT_COIN;
                else
                    displayState = CURRENT_AMOUNT;
                return SOLD_OUT_MESSAGE;
            case PRODUCT_PRICE:
                if (register.GetCoinsValueInReservoir(CREDIT) == 0.f)
                    displayState = INSERT_COIN;
                else
                    displayState = CURRENT_AMOUNT;
                float price = 0.f;
                switch (prevKeyStrike) {
                    case Key_Cola:
                    case Key_Chips:
                    case Key_Candy:
                        String product = Product_Keys.get(prevKeyStrike);
                        price = Products.get(product);
                        break;
                    default:
                        break;

                }
                return String.format(PRODUCT_PRICE_MESSAGE_FORMAT,
                        DecimalFormat.getCurrencyInstance().format(price));
            default:
                return MACHINE_ERROR;
        }

    }


    /**
     * The customer gets the purchased item from the dispersion
     *
     * @return the purchased product, EMPTY if nothing there.
     */
    public String GetDispersion() {
        String tmpDisperstion = dispersion;
        dispersion = Empty;
        return tmpDisperstion;
    }

    /**
     * Whether the display should show INSERT COIN or EXACT CHANGE depending on if there are enough coins to make change
     * for any of the product.
     *
     * @return INSERT COIN if there are enough coins to make change, otherwise EXACT CHANGE.
     */
    private String ShowInsertOrExactChange() {
        return register.RequireExactChange() ? EXACT_CHANGE_MESSAGE : INSERT_COIN_MESSAGE;
    }

    //endregion

    //region clear up and reload

    public void PreloadReserve(Coin coin, int quantity) {
        register.PreloadRegister(coin, quantity);
    }

    public void InsertCoins(Coin coin, int quantity) {
        register.InsertCoins(coin, quantity);
        displayState = CURRENT_AMOUNT;
    }

    /**
     * Empty the inventory.
     */
    public void ClearInventories() {
        for (String product : Products.keySet())
            inventories.put(product, 0);
    }

    /**
     * Load product to the machine inventories.
     *
     * @param refills products stored in a hashmap that maps product type to quantities of the product.
     */
    public void ReloadInventories(Map<String, Integer> refills) {
        Iterator iter = refills.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry pair = (Map.Entry) iter.next();
            inventories.put((String) pair.getKey(), inventories.get(pair.getKey()) + (Integer) pair.getValue());
        }
    }

    //endregion
}

