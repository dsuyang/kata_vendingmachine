import com.sun.org.apache.regexp.internal.RE;
import com.sun.org.apache.xml.internal.security.c14n.CanonicalizationException;

import java.util.*;

/**
 * Created by dsuyang on 4/12/17.
 * This class handles the reserve, credit and return of the register.
 */
public class Register implements Constants {
    private HashMap<String, Integer> reserves = new HashMap<String, Integer>();
    private HashMap<String, Integer> credits = new HashMap<String, Integer>();
    private HashMap<String, Integer> returns = new HashMap<String, Integer>();
    private HashMap<String, HashMap<String, Integer>> reservoirs = new HashMap<>();

    public Register() {
        reservoirs.put(RESERVE, reserves);
        reservoirs.put(CREDIT, credits);
        reservoirs.put(RETURN, returns);
    }

    public void PreloadRegister(Coin coin, int quantity) {
        String type = Coin.recognize_coin(coin);
        incrementOfCoins(reserves, type, quantity);
    }

    public void ClearRegister() {
        for (Map<String, Integer> reservoir : reservoirs.values())
            reservoir.clear();
    }

    public boolean InsertCoins(Coin coin, int quantity) {
        String type = Coin.recognize_coin(coin);
        if (VALID_COIN_TYPES.contains(type)) {
            incrementOfCoins(credits, type, quantity);
            return true;
        } else {
            incrementOfCoins(returns, type, quantity);
            return false;
        }
    }

    private void incrementOfCoins(Map<String, Integer> reservoir, String coinType, int quantity) {
        if (reservoir.get(coinType) == null)
            reservoir.put(coinType, quantity);
        else
            reservoir.put(coinType, reservoir.get(coinType) + quantity);
    }

    public Map<String, Integer> GetCoinsInReservoir(String reservoirType) {
        // create a temp to protect the data member from being modified
        Map tmp = new HashMap<>();
        reservoirs.get(reservoirType).forEach(tmp::putIfAbsent);
        return tmp;
    }

    public float GetCoinsValueInReservoir(String type) {
        float total = 0.f;
        Map<String, Integer> coins = GetCoinsInReservoir(type);

        for (String coinType : coins.keySet())
            total += Coin.recognize_coin_value(coinType) * coins.get(coinType);
        return total;
    }

    public boolean Buy(String type) {
        float changeAmount = GetCoinsValueInReservoir(CREDIT) - Products.get(type);

        try {
            MakeChanges(changeAmount);
        } catch (CannotMakeChangeException e) {
            return false;
        } finally {
            TransferCredits(reserves);
        }
        return true;
    }

    private void TransferCredits(Map<String, Integer> dest) {
        for (String coinType : credits.keySet())
            incrementOfCoins(dest, coinType, credits.get(coinType));
        credits.clear();
    }

    private void MakeChanges(float amount) throws CannotMakeChangeException {
        // avoiding very tricky rounding issue...
        amount += RESIDUAL;
        for (int i = 0; i < VALID_COIN_TYPES.size(); ++i) {
            while (amount > VALID_COINT_VALUES.get(i) && RetrieveACoin(VALID_COIN_TYPES.get(i))) {
                incrementOfCoins(returns, VALID_COIN_TYPES.get(i), 1);
                amount -= VALID_COINT_VALUES.get(i);
            }
        }

        if (amount >= RESIDUAL)
            throw new CannotMakeChangeException(amount);
    }

    /**
     * Retrieve a coin from the credit or the register, and subtract the corresponding amount.
     * It always tries to retrieve from the credit first. If not successful, then retrieve from the reserve.
     *
     * @param type the type of the coin to be retrieved.
     * @return false if no coin of the specified type can be found in neither credit or register.
     */
    private boolean RetrieveACoin(String type) {
        if (credits.get(type) != null && credits.get(type) > 0) {
            credits.put(type, credits.get(type) - 1);
            return true;
        } else if (reserves.get(type) != null && reserves.get(type) > 0) {
            reserves.put(type, reserves.get(type) - 1);
            return true;
        } else {
            return false;
        }
    }

    public void ReturnCredits() {
        TransferCredits(returns);
    }

    /**
     * Are there enough coins in the register of the machine to make changes for any of the product?
     *
     * @return true if there are NOT enough coins to make the change.
     */
    protected boolean RequireExactChange() {
        // It is not that hard prove that the machine needs either 2 Nickles or 1 Nickle and 1 Dime
        // to be able to break changes.

        if (reserves.get(NICKEL) != null && reserves.get(NICKEL) >= 2)
            return false;
        else if (reserves.get(NICKEL) != null && reserves.get(DIME) != null &&
                reserves.get(NICKEL) == 1 && reserves.get(DIME) >= 1)
            return false;

        return true;

    }
}
