import java.text.DecimalFormat;

/**
 * Created by dsuyang on 3/30/17.
 * The exception is thrown when no coins are available to make the change.
 */
public class CannotMakeChangeException extends Exception{
    public CannotMakeChangeException(final String msg)
    {
        super(msg);
    }

    public CannotMakeChangeException(float amount)
    {
        this(String.format("Cannot make change of the amount %s",
            DecimalFormat.getCurrencyInstance().format(amount)));
    }
}
