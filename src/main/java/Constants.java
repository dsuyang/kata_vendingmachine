import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsuyang on 3/30/17.
 * This interface contains all the constants that will be used throughout the project
 */
public interface Constants {
    // Display Message State Machine
    public static final String INSERT_COIN = "vendingmachine.display.statemachine.insertcoin";
    public static final String CURRENT_AMOUNT = "vendingmachine.display.statemachine.currentamount";
    public static final String PRODUCT_PRICE = "vendingmachine.display.statemachine.productprice";
    public static final String THANK_YOU = "vendingmachine.display.statemachine.thankyou";
    public static final String SOLD_OUT = "vendingmachine.display.statemachine.soldout";
    public static final String MACHINE_ERROR = "vendingmachine.display.statemachine.error";

    public static final String INSERT_COIN_MESSAGE = "INSERT COIN";
    public static final String EXACT_CHANGE_MESSAGE = "EXACT CHANGE ONLY";
    public static final String THANK_YOU_MESSAGE = "THANK YOU";
    public static final String SOLD_OUT_MESSAGE = "SOLD OUT";
    public static final String CURRENT_AMOUNT_MESSAGE_FORMAT = "CURRENT AMOUNT: %2s";
    public static final String PRODUCT_PRICE_MESSAGE_FORMAT = "PRICE: %2s";

    // Keyboard
    public static final String Key_Cola = "vendingmachine.keyboard.cola";
    public static final String Key_Chips = "vendingmachine.keyboard.chips";
    public static final String Key_Candy = "vendingmachine.keyboard.candy";
    public static final String Key_Return = "vendingmachine.keyboard.return";

    // Product
    public static final String Cola = "vendingmachine.product.cola";
    public static final float Cola_Price = 1.f;
    public static final int Cola_Init_Inventory = 3;
    public static final String Chips = "vendingmachine.product.chips";
    public static final float Chips_Price = 0.5f;
    public static final int Chips_Init_Inventory = 3;
    public static final String Candy = "vendingmachine.product.candy";
    public static final float Candy_Price = 0.65f;
    public static final int Candy_Init_Inventory = 3;
    public static final String Empty = "vendingmachine.product.nothing";
    public static final Map<String, Float> Products = new HashMap<String, Float>(){{put(Cola, Cola_Price); put(Chips, Chips_Price);
                                                        put(Candy, Candy_Price);}};
    public static final Map<String, String> Product_Keys = new HashMap<String, String>(){{put(Key_Cola, Cola); put(Key_Chips, Chips);
        put(Key_Candy, Candy);}};

    // Coin Characteristics
    public static final String NICKEL = "coin.nickel";
    public static final String NICKEL_WEIGHT = "coin.nickle.weight";
    public static final String NICKEL_SIZE = "coin.nickle.size";
    public static final float NICKEL_VALUE = 0.05f;
    public static final int NICKLE_INIT_REGISTER = 10;
    public static final String DIME = "coin.dime";
    public static final String DIME_WEIGHT = "coin.dime.weight";
    public static final String DIME_SIZE = "coin.dime.size";
    public static final float DIME_VALUE = 0.1f;
    public static final int DIME_INIT_REGISTER = 10;
    public static final String QUARTER = "coin.quarter";
    public static final String QUARTER_WEIGHT = "coin.quarter.weight";
    public static final String QUARTER_SIZE = "coin.quarter.size";
    public static final float QUARTER_VALUE = 0.25f;
    public static final int QUARTER_INIT_REGISTER = 10;
    public static final String PENNY = "coin.penny";
    public static final String PENNY_WEIGHT = "coin.penny.weight";
    public static final String PENNY_SIZE = "coin.penny.size";
    public static final String FAKE = "coin.fake";
    public static final String UNDEFINED_WEIGHT = "coin.undefined.weight";
    public static final String UNDEFINED_SIZE = "coin.undefined.size";
    public static final List<String> VALID_COIN_TYPES = Arrays.asList(QUARTER, DIME, NICKEL);
    public static final List<Float> VALID_COINT_VALUES = Arrays.asList(QUARTER_VALUE, DIME_VALUE, NICKEL_VALUE);

    public static final float RESIDUAL = 0.001f;

    // Register
    public static final String RESERVE = "register.type.reserve";
    public static final String CREDIT = "register.type.credit";
    public static final String RETURN = "register.type.return";


}
