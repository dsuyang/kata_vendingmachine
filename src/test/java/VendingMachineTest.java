import com.sun.tools.internal.jxc.ap.Const;
import org.junit.Before;
import org.junit.Test;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
/**
 * Created by dsuyang on 4/15/17.
 */
public class VendingMachineTest extends TestRules implements Constants {
    private VendingMachine vm;
    @Before
    public void InitVendingMachine()
    {
        vm = new VendingMachine();
        Map<String, Integer> stock = new HashMap<String, Integer>(){{
            put(Cola, Cola_Init_Inventory); put(Chips, Chips_Init_Inventory);
            put(Candy, Candy_Init_Inventory);
        }};
        vm.ReloadInventories(stock);
    }
    //region Display Insert, Exact, and Amount
    @Test
    public void DisplayInsertMessageWhenNoCoinsInserted() {
        vm.PreloadReserve(aDime, 2);
        vm.PreloadReserve(aNickle, 2);
        assertThat(vm.GetDisplay(), is(INSERT_COIN_MESSAGE));
    }

    @Test
    public void DisplayExactChangesMessageWhenNoCoinsInserted() {
        assertThat(vm.GetDisplay(), is(EXACT_CHANGE_MESSAGE));
    }
    @Test
    public void DisplayCurrentAmountAfterInsertCoins()
    {
        vm.InsertCoins(aNickle, 1);
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(0.05))));
        vm.InsertCoins(aDime, 1);
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(0.15))));
        vm.InsertCoins(aQuarter, 1);
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(0.4))));
    }
    //endregion

    //region Select product
    @Test
    public void DisplayThankYouMessageAfterSelectCola() {
        // insert 4 quarters to buy a Cola which costs 1 dollar.
        vm.InsertCoins(aQuarter, 4);
        vm.PressKeyboard(Key_Cola);
        assertThat(vm.GetDispersion(), is(Cola));
        assertThat(vm.GetDisplay(), is(THANK_YOU_MESSAGE));
        assertThat(vm.GetDisplay(), is(EXACT_CHANGE_MESSAGE));
    }

    @Test
    public void DisplayThankYouMessageAfterSelectChips() {
        // insert 2 quarters to buy Chips which costs 50 cents.
        vm.InsertCoins(aQuarter, 2);
        vm.PressKeyboard(Constants.Key_Chips);
        assertThat(vm.GetDispersion(), is(Chips));
        assertThat(vm.GetDisplay(), is(THANK_YOU_MESSAGE));
        assertThat(vm.GetDisplay(), is(EXACT_CHANGE_MESSAGE));
    }

    @Test
    public void DisplayThankYouMessageAfterSelectCandy() {
        // insert 65 cents to buy Candy which costs 65 cents.
        vm.InsertCoins(aQuarter, 2);
        vm.InsertCoins(aDime, 1);
        vm.InsertCoins(aNickle, 1);
        vm.PressKeyboard(Constants.Key_Candy);
        assertThat(vm.GetDispersion(), is(Candy));
        assertThat(vm.GetDisplay(), is(THANK_YOU_MESSAGE));
        // the register has one dime and one nickle now, and has enough change
        assertThat(vm.GetDisplay(), is(INSERT_COIN_MESSAGE));
    }
    //endregion

    //region Not enough credits
    @Test
    public void DisplayProductPriceMessageWhenNoCredits() {
        vm.PressKeyboard(Key_Cola);
        assertThat(vm.GetDispersion(), is(Empty));
        assertThat(vm.GetDisplay(), is(String.format(PRODUCT_PRICE_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(Cola_Price))));
    }

    @Test
    public void DisplayProductPriceMessageWhenNotEnoughCredits() {
        // insert 50 cents, but want a Cola, which is not enough.
        vm.InsertCoins(aQuarter, 2);
        vm.PressKeyboard(Key_Cola);
        assertThat(vm.GetDispersion(), is(Empty));
        assertThat(vm.GetDisplay(), is(String.format(PRODUCT_PRICE_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(Cola_Price))));
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(0.5f))));
    }
    //endregion

    //region Sold out
    @Test
    public void DisplaySoldOutWhenNoInventory(){
        vm.ClearInventories();
        vm.InsertCoins(aQuarter, 3);
        vm.PressKeyboard(Constants.Key_Candy);
        assertThat(vm.GetDisplay(), is(SOLD_OUT_MESSAGE));
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(0.75f))));
    }

    @Test
    public void DisplaySoldOutAfterSellingLastSoda(){

        vm.ClearInventories();
        // Load one Cola into the machine.
        Map<String, Integer> stock = new HashMap<String, Integer>(){{
            put(Cola,1); }};
        vm.ReloadInventories(stock);

        // Buy one Cola.
        vm.InsertCoins(aQuarter, 4);
        vm.PressKeyboard(Constants.Key_Cola);
        assertThat(vm.GetDispersion(), is(Cola));
        assertThat(vm.GetDisplay(), is(THANK_YOU_MESSAGE));
        assertThat(vm.GetDisplay(), is(EXACT_CHANGE_MESSAGE));

        // Try to buy another but now sold out.
        vm.InsertCoins(aQuarter, 4);
        vm.PressKeyboard(Constants.Key_Cola);
        assertThat(vm.GetDisplay(), is(SOLD_OUT_MESSAGE));
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(1.f))));
    }

    @Test
    public void DisplaySoldOutAfterSellingAllSoda(){
        for (int i = 0; i < Cola_Init_Inventory; ++i) {
            vm.InsertCoins(aQuarter, 4);
            vm.PressKeyboard(Constants.Key_Cola);
            assertThat(vm.GetDispersion(), is(Cola));
            assertThat(vm.GetDisplay(), is(THANK_YOU_MESSAGE));
            assertThat(vm.GetDisplay(), is(EXACT_CHANGE_MESSAGE));
        }

        // Try to buy one more, but now sold out.
        vm.InsertCoins(aQuarter, 4);
        vm.PressKeyboard(Constants.Key_Cola);
        assertThat(vm.GetDisplay(), is(SOLD_OUT_MESSAGE));
        assertThat(vm.GetDisplay(), is(String.format(CURRENT_AMOUNT_MESSAGE_FORMAT,
                DecimalFormat.getCurrencyInstance().format(1.f))));
    }
    //endregion
}