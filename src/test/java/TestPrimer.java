import junit.framework.TestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by dsuyang on 3/30/17.
 * Run all the test suites
 */

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                VendingMachineTest.class,
                RegisterTest.class
        }
)
public class TestPrimer {
}
