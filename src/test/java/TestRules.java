import org.junit.Before;
import org.junit.BeforeClass;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dsuyang on 3/30/17.
 * The common code of all the test suites.
 */
public class TestRules implements Constants {

    protected static Coin aNickle;
    protected static Coin aDime;
    protected static Coin aQuarter;
    @BeforeClass
    public static void Init()
    {
        aNickle = Coin.make_coin(Constants.NICKEL);
        aDime = Coin.make_coin(Constants.DIME);
        aQuarter = Coin.make_coin(Constants.QUARTER);
    }
}
