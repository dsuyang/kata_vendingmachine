import com.sun.tools.corba.se.idl.constExpr.Equal;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by dsuyang on 4/12/17.
 */
public class RegisterTest extends TestRules{
    protected Register register;
    @Before
    public void InitRegister() {
        register = new Register();
        register.PreloadRegister(aQuarter, 10);
        register.PreloadRegister(aDime, 10);
        register.PreloadRegister(aNickle, 10);
    }

    //region Validate inserted coins
    @Test
    public void AcceptValidCoins()
    {
        assertThat(register.InsertCoins(aNickle,1), is(true));
        assertThat(register.InsertCoins(aDime,1), is(true));
        assertThat(register.InsertCoins(aQuarter,1), is(true));
    }

    @Test
    public void ReturnInvalidCoins()
    {
        Coin aPenny = Coin.make_coin(Constants.PENNY);
        assertThat(register.InsertCoins(aPenny,1), is(false));
        assertThat(register.GetCoinsInReservoir(RETURN).get(PENNY), is(equalTo(1)));

        Coin aFake = Coin.make_coin(Constants.FAKE);
        assertThat(register.InsertCoins(aFake, 1), is(false));
        assertThat(register.GetCoinsInReservoir(RETURN).get(FAKE), is(equalTo(1)));

    }

    @Test
    public void AuditCurrentCredit()
    {
        register.InsertCoins(aNickle, 1);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(CREDIT) - 0.05f) < RESIDUAL);
        register.InsertCoins(aDime, 1);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(CREDIT) - 0.15f) < RESIDUAL);
        register.InsertCoins(aQuarter, 1);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(CREDIT) - 0.4f) < RESIDUAL);
        Coin aFake = Coin.make_coin(Constants.FAKE);
        register.InsertCoins(aFake, 1);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(CREDIT) - 0.4f) < RESIDUAL);
    }
    //endregion

    //region Return changes
    @Test
    public void ReturnDimeWhenCreditsMoreThanProductPrice()
    {
        register.InsertCoins(aQuarter, 3);
        register.Buy(Candy);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.1f) < RESIDUAL);
    }

    @Test
    public void ReturnNickleWhenCreditsMoreThanProductPrice()
    {
        register.InsertCoins(aQuarter, 2);
        register.InsertCoins(aDime, 2);
        register.Buy(Candy);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.05f) < RESIDUAL);
    }

    @Test
    public void ReturnQuarterWhenCreditsMoreThanProductPrice()
    {
        register.InsertCoins(aDime, 9);
        register.Buy(Candy);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.25f) < RESIDUAL);
    }

    @Test
    public void ReturnDimeAndNickleWhenCreditsMoreThanProductPrice()
    {
        register.InsertCoins(aDime, 8);
        register.Buy(Candy);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.15f) < RESIDUAL);
    }


    @Test
    public void ReturnNoChangeWhenCreditsEqualsToProductPrice()
    {
        register.InsertCoins(aNickle, 10);
        register.Buy(Chips);
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.f) < RESIDUAL);
    }
    //endregion

    //region Return credits

    @Test
    public void ReturnNoCoinsWhenNoCredits() {
        // Edge case: return nothing.
        register.ReturnCredits();
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.f) < RESIDUAL);
    }

    @Test
    public void ReturnAllCredits() {
        register.InsertCoins(aQuarter, 2);
        register.InsertCoins(aDime, 2);
        register.InsertCoins(aNickle, 2);
        register.ReturnCredits();
        assertTrue(Math.abs(register.GetCoinsValueInReservoir(RETURN) - 0.8f) < RESIDUAL);
    }
    //endregion

    //region Exact change
    @Test
    public void InsertCoinsWhenReserveHasTwoNickles()
    {
        register.ClearRegister();
        register.PreloadRegister(aNickle, 2);
        assertThat(register.RequireExactChange(), is(false));

    }

    @Test
    public void InsertCoinsWhenReserveHasOneDimeOneNickle()
    {
        register.ClearRegister();
        register.PreloadRegister(aNickle, 1);
        register.PreloadRegister(aDime, 1);
        assertThat(register.RequireExactChange(), is(false));
    }

    @Test
    public void ExactChangeWhenReserveHasOneDime()
    {
        register.ClearRegister();
        register.PreloadRegister(aDime, 1);
        assertThat(register.RequireExactChange(), is(true));
    }

    @Test
    public void ExactChangeWhenReserveHasOneNickle()
    {
        register.ClearRegister();
        register.PreloadRegister(aNickle, 1);
        assertThat(register.RequireExactChange(), is(true));
    }

    //endregion


}
